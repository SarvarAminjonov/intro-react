import React, { useState, useEffect }  from "react";
import './App.css';
import axios from "axios"
import Product from "./Components/Products/newProduct";
import Header from "./Components/Header/header";


function App() {
  const[product,setProduct] = useState([])
  const[products,setProducts] = useState([])
  
const getProduct = () =>{
  axios.get('https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/products')
       .then(res => {
         console.log(res.data);
         setProduct(res.data)
       })
}
const getProducts = () =>{
  axios.get('https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/products')
        .then(resp => {
          setProducts(resp.data)
        })
}
useEffect(() => {
  getProduct()
  getProducts()
}, [])

const [show, setShow] = useState(false);

const productAll = products.slice(0,6).map((products)=>(
  
    <div key={products.id} className='card'>
        <div>
     <div className="img-box">
     <img
    className="image"
    src={products?.image}
    alt={products?.title}
    />
     </div>
     <div className="product-info">
     <p>&#11088;&#11088;&#11088;&#11088;&#11088;</p>
        <p>{products.name}</p>
            <p>{products && products.old_price? products.old_price + '$':"Chegirmasiz"}</p>
            <p className="price">{products.price} $</p>
            <p>{products && products.monthly_pay ? products.monthly_pay + '$ Muddatli to\'lov':'Mudatli to\'lovsiz'}</p>
            <button className={products.in_stock === 0 ? "btn-disabled":'btn' }> В корзину</button>
     </div>
     
     </div> 
    </div>
   
));
const productss = products.map((products)=>(
    <div key={products.id} className='card'>
        <div>
      
     <div className="img-box">
     <img
    className="image"
    src={products?.image}
    alt={products?.title}
    />
     </div>
     <div className="product-info">
     <p>&#11088;&#11088;&#11088;&#11088;&#11088;</p>
        <p>{products.name}</p>
            <p>{products && products.old_price? products.old_price + '$':"Chegirmasiz"}</p>
            <p className="price">{products.price} $</p>
            <p>{products && products.monthly_pay ? products.monthly_pay + '$ Muddatli to\'lov':'Mudatli to\'lovsiz'}</p>
            <button className={products.in_stock === 0 ? "btn-disabled":'btn' }> В корзину</button>
     </div>
     
     </div> 
    </div>
));
  return (
  
    <div className="App">
            <Header/> 
						<Product product={product} />
            <>
            <div className="wrap">
        <div>
            <div className="sec2">
            <div>
                <h1 className="header_text">Выбор покупателей</h1>
            </div>
            <div>
            <span onClick={() => setShow(!show)}>
              {!show ? "Смотреть все" : "Скрыть"}
            </span>
            </div>
            </div>
                <div className="contain">
                    {show ? productss : productAll}
                </div>
        </div>
        </div>
        </>
    </div>
  );
}
  


export default App;
