import React from "react";
import "./header.css";
import logo from "../image/MaxProff.svg"
import healt from "../image/heart_Shape (1).svg"
import diag from "../image/Vector (2).svg"
import korz from "../image/korz.svg"
import pers from "../image/person.svg"
import search from "../image/Vector.svg"



const Header = () => {
    return(
        <>
        <div className="wraper">
        <div className="header-wrap">
            <div className="header-logo">
                <img src={logo} alt="" />
            </div>
            <div className="form">
                <div className="inp">
                    <input type="text" className="input-search" placeholder="Искать товары"/>
                </div>
                <div className="search">
                    <button className="search-btn"><img src={search} alt="" /></button>
                </div>
            </div>
            <div className="menu">
                <div className="menu-nav">
                    <div className="nav-img">
                        <img src={healt} alt="" />
                    </div>
                    <p>Избранное</p>
                </div>
                <div className="menu-nav">
                    <div className="nav-img">
                        <img src={diag} alt="" />
                    </div>
                    <p>Сравнение</p>
                </div>
                <div className="menu-nav">
                    <div className="nav-img">
                        <img src={korz} alt="" />
                    </div>
                    <p>Избранное</p>
                </div>
                <div className="menu-nav">
                    <div className="nav-img">
                        <img src={pers} alt="" />
                    </div>
                    <p>Избранное</p>
                </div>
            </div>

        </div>
        <div className="nav">
            <ul>
                <li>Акции и скидки</li>
                <li>Ноутбуки и компьютеры</li>
                <li>Смартфоны и гаджеты</li>
                <li>Телевизоры и аудио</li>
                <li>Красота и здоровье</li>
                <li>Техника для дома</li>
            </ul>
        </div>
        </div>
        </>
    )

}
export default Header