import  React from "react";
import "./newProduct.css"



const newProduct = ({product}) => {
    const newProduct = product.filter(item => item.status ==="new");
    return(
        <>
        <div className="wrap">
        <h1 className="header_text">Новинки</h1>
        <div className="contain">
          
           {newProduct.map((product)=>(
         <div key={product.id} className='card'>
         <div className="img-box">
         <img
        className="image"
        src={product?.image}
        alt={product?.title}
        />
         </div>
         <div className="product-info">
         <p>&#11088;&#11088;&#11088;&#11088;&#11088;</p>
            <p>{product.name}</p>
                <p>{product && product.old_price? product.old_price + '$':"Chegirmasiz"}</p>
                <p className="price">{product.price} $</p>
                <p>{product && product.monthly_pay ? product.monthly_pay + '$ Muddatli to\'lov':'Mudatli to\'lovsiz'}</p>
                <button className="btn">В корзину</button>
         </div>
         
         </div> ))}
      
      
        </div>
        </div>
        </>
    )
}

export default newProduct